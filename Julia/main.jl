using Pkg, DataFrames, DelimitedFiles, LaTeXStrings, FileIO, XLSX, Gadfly, Cairo, Compose, GLM, Roots, Fontconfig
latex_fonts = Theme(major_label_font="Helvetica", major_label_font_size=12pt,
                   minor_label_font="Helvetica", minor_label_font_size=12pt,
                   key_title_font="Helvetica", key_title_font_size=12pt,
                   key_label_font="Helvetica", key_label_font_size=12pt, background_color=colorant"white", line_width=1px, key_position=:inside, default_color=colorant"blue")
Gadfly.push_theme(latex_fonts)
## load data
CuFoil = readdlm("/home/ezra/Downloads/Student_Data/Cu_Foil.xy", Float64, skipstart=2)
Cu26Ni74 = readdlm("/home/ezra/Downloads/Student_Data/Cu26Ni74_pwdr.xy", Float64, skipstart=2)
Cu63Ni37 = readdlm("/home/ezra/Downloads/Student_Data/Cu63Ni37_foil.xy", Float64, skipstart=2)
CuNiUk = readdlm("/home/ezra/Downloads/Student_Data/CuNi_Unknown_Foil.xy", Float64, skipstart=2)
NiFoil = readdlm("/home/ezra/Downloads/Student_Data/Ni_Foil.xy", Float64, skipstart=2)

ref_data = DataFrame(Ni_ref_angle=XLSX.readdata("Data/XRD_Standards.xlsx", "Ni_std", "B30:B37")[:,1], Ni_ref_hkl=replace(XLSX.readdata("Data/XRD_Standards.xlsx", "Ni_std", "E30:E37")[:,1], " " => ""), Cu_ref_angle=XLSX.readdata("Data/XRD_Standards.xlsx", "Cu_std", "B30:B37")[:,1], Cu_ref_hkl=replace(XLSX.readdata("Data/XRD_Standards.xlsx", "Cu_std", "E30:E37")[:,1], " " => ""))

planes = [1 1 1; 2 0 0; 2 2 0; 3 1 1; 2 2 2; 4 0 0; 3 3 1; 4 2 0]
peaks = DataFrame(Cu_angles=[43.3256,50.4071,74.105,89.8716,95.1253,116.7618], C6N3_angles=[43.6629, 50.8727, 74.8569, 90.9075, 96.2104, 118.5825], C2N7_angles=[44.0274, 51.3403, 75.6535, 92.0137, 97.4548, missing], Ni_angles=[44.3753, 51.7256, 76.2595, 92.7877, 98.3305, 121.7872], unknown_angles=[43.895, 51.1754, 75.3833, 91.6447, 97.0304, 119.904])
## Data Work

##lattice parameter
lambda = 1.54059
# sample represents a column in the peaks dataframe, number is the plane number of the sample for which we want the lattice parameter
param(sample,number) = (lambda * sqrt(sum(.^(planes[number,:], 2)))) / (2 * sind(peaks[number,sample] / 2))
Cu_data = DataFrame(peak_angles=[43.3256,50.4071,74.105,89.8716,95.1253,116.7618], params=param.(1, [1:6;]))
Cu63Ni37_data = DataFrame(peak_angles=[43.6629, 50.8727, 74.8569, 90.9075, 96.2104, 118.5825], params=param.(2, [1:6;]))
Cu26Ni74_data = DataFrame(peak_angles=[44.0274, 51.3403, 75.6535, 92.0137, 97.4548], params=param.(3, [1:5;]))
Ni_data = DataFrame(peak_angles=[44.3753, 51.7256, 76.2595, 92.7877, 98.3305, 121.7872], params=param.(4, [1:6;]))
unknown_data = DataFrame(peak_angles=[43.895, 51.1754, 75.3833, 91.6447, 97.0304, 119.904], params=param.(5, [1:6;]))

# calculate the x value for each lattice paramter and put it in the respective column of the dataframe
Cu_data.xvals = (.^(cosd.(peaks.Cu_angles ./ 2), 2) ./ ((sind.(peaks.Cu_angles ./ 2))))
Cu63Ni37_data.xvals = (.^(cosd.(peaks.C6N3_angles ./ 2), 2) ./ ((sind.(peaks.C6N3_angles ./ 2))))
Cu26Ni74_data.xvals = (.^(cosd.(Cu26Ni74_data.peak_angles ./ 2), 2) ./ ((sind.(Cu26Ni74_data.peak_angles ./ 2))))
Ni_data.xvals = (.^(cosd.(Ni_data.peak_angles ./ 2), 2) ./ ((sind.(Ni_data.peak_angles ./ 2))))
unknown_data.xvals = (.^(cosd.(unknown_data.peak_angles ./ 2), 2) ./ ((sind.(unknown_data.peak_angles ./ 2))))

# regressions for lattice parameters
Cu_reg = lm(@formula(params ~ xvals), Cu_data)
Cu63Ni37_reg = lm(@formula(params ~ xvals), Cu63Ni37_data)
Cu26Ni74_reg = lm(@formula(params ~ xvals), Cu26Ni74_data)
Ni_reg = lm(@formula(params ~ xvals), Ni_data)
unknown_reg = lm(@formula(params ~ xvals), unknown_data)

Cu_param_func(x) = x * coef(Cu_reg)[2] + coef(Cu_reg)[1]
Cu63Ni37_param_func(x) = x * coef(Cu63Ni37_reg)[2] + coef(Cu63Ni37_reg)[1]
Cu26Ni74_param_func(x) = x * coef(Cu26Ni74_reg)[2] + coef(Cu26Ni74_reg)[1]
Ni_param_func(x) = x * coef(Ni_reg)[2] + coef(Ni_reg)[1]
unknown_param_func(x) = x * coef(unknown_reg)[2] + coef(unknown_reg)[1]

# make a dataframe with parameters and their respective x value of Cu(1-x)Nix
param_frame = DataFrame(x=[0, .37, .74, 1], params=[coef(Cu_reg)[1], coef(Cu63Ni37_reg)[1], coef(Cu26Ni74_reg)[1], coef(Ni_reg)[1]])
vegard_reg = lm(@formula(params ~ x), param_frame)
vegard_func(x) = x * coef(vegard_reg)[2] + coef(vegard_reg)[1]

# calculate the mol% Ni of the unknown
unknown_param_find(x) = x * coef(vegard_reg)[2] + coef(vegard_reg)[1] - coef(unknown_reg)[1]
unknown_Ni_mol = find_zero(unknown_param_find, .5)

# plotting lattice parameters
Cu_params_plot = plot(layer(Cu_data, x="xvals", y="params", Geom.point), layer(Cu_param_func, 0, 2.5), Guide.xlabel("cos<sup>2</sup>(θ)/sin(θ)"), Guide.ylabel("Lattice Parameter (Å)"))
Cu63Ni37_params_plot = plot(layer(Cu63Ni37_data, x="xvals", y="params", Geom.point), layer(Cu63Ni37_param_func, 0, 2.5), Guide.xlabel("cos<sup>2</sup>(θ)/sin(θ)"), Guide.ylabel("Lattice Parameter (Å)"))
Cu26Ni74_params_plot = plot(layer(Cu26Ni74_data, x="xvals", y="params", Geom.point), layer(Cu26Ni74_param_func, 0, 2.5), Guide.xlabel("cos<sup>2</sup>(θ)/sin(θ)"), Guide.ylabel("Lattice Parameter (Å)"))
Ni_params_plot = plot(layer(Ni_data, x="xvals", y="params", Geom.point), layer(Ni_param_func, 0, 2.5), Guide.xlabel("cos<sup>2</sup>(θ)/sin(θ)"), Guide.ylabel("Lattice Parameter (Å)"))
unknown_params_plot = plot(layer(unknown_data, x="xvals", y="params", Geom.point), layer(unknown_param_func, 0, 2.5), Guide.xlabel("cos<sup>2</sup>(θ)/sin(θ)"), Guide.ylabel("Lattice Parameter (Å)"))

draw(PDF("Cu_params.pdf"),Cu_params_plot)
draw(PDF("Cu63Ni37_params.pdf"),Cu63Ni37_params_plot)
draw(PDF("Cu26Ni74_params.pdf"),Cu26Ni74_params_plot)
draw(PDF("Ni_params.pdf"),Ni_params_plot)
draw(PDF("unknown_params.pdf"),unknown_params_plot)

# plot the parameters and their x value
params_plot = plot(layer(param_frame, x="x", y="params"), Guide.xticks(ticks=[0:0.1:1;]), Guide.xlabel("mol% Ni"), Guide.ylabel("Extrapolated Lattice Parameter (Å)"), layer(vegard_func, 0, 1), layer(x=[unknown_Ni_mol], y=[coef(unknown_reg)[1]], color=[colorant"red"], Geom.point))

#plot parameters of known samples only
known_params_plot = plot(layer(param_frame, x="x", y="params"), Guide.xticks(ticks=[0:0.1:1;]), Guide.xlabel("mol% Ni"), Guide.ylabel("Extrapolated Lattice Parameter (Å)"), layer(vegard_func, 0, 1))
draw(PDF("known_params.pdf"),known_params_plot)

## plotting XRD patterns in Gadfly

Cu_plot = Gadfly.plot(x=CuFoil[:,1], y=CuFoil[:,2], Geom.line, Coord.cartesian(xmin=30), Guide.xticks(ticks=[30:10:130;]), Guide.yticks(ticks=[0:5000:2.1e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Cu Foil"], [colorant"blue"], pos=[0.76w,-.43h]), Guide.annotation(compose(context(), Compose.text(peaks.Cu_angles, [1.9e4,1.35e4,7e3,5e3,2e3,2e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))
C63Ni37_plot = Gadfly.plot(x=Cu63Ni37[:,1], y=Cu63Ni37[:,2], Geom.line, Coord.cartesian(xmin=30, ymax=2.1e4), Guide.xticks(ticks=[30:10:130;]), Guide.yticks(ticks=[0:5000:2.5e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Cu<sub>63</sub>Ni<sub>37</sub> Foil"], [colorant"blue"], pos=[0.65w,-.4h]), Guide.annotation(compose(context(), Compose.text(peaks.C6N3_angles, [2e4,1.1e4,7.5e3,4.5e3,2e3,2e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))
Cu26Ni74_plot = Gadfly.plot(x=Cu26Ni74[:,1], y=Cu26Ni74[:,2], Geom.line, Coord.cartesian(xmin=30, ymax=2.1e4), Guide.xticks(ticks=[30:10:105;]), Guide.yticks(ticks=[0:5000:2.5e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Cu<sub>26</sub>Ni<sub>74</sub> Powder"], [colorant"blue"], pos=[0.6w,-.4h]), Guide.annotation(compose(context(), Compose.text(peaks.C2N7_angles, [1.5e4,6e3,3e3,3e3,1.5e3,2e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))
Ni_plot = Gadfly.plot(x=NiFoil[:,1], y=NiFoil[:,2], Geom.line, Coord.cartesian(xmin=30, ymax=2.1e4), Guide.xticks(ticks=[30:10:130;]), Guide.yticks(ticks=[0:5000:2.5e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Ni Foil"], [colorant"blue"], pos=[0.76w,-.43h]), Guide.annotation(compose(context(), Compose.text(peaks.Ni_angles, [2e4,1.1e4,7.5e3,4.5e3,2e3,2e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))
unknown_plot = Gadfly.plot(x=CuNiUk[:,1], y=CuNiUk[:,2], Geom.line, Coord.cartesian(xmin=30, ymax=2.1e4), Guide.xticks(ticks=[30:10:130;]), Guide.yticks(ticks=[0:5000:2.5e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Cu<sub>x</sub>Ni<sub>1-x</sub> Foil"], [colorant"blue"], pos=[0.65w,-.4h]), Guide.annotation(compose(context(), Compose.text(peaks.C6N3_angles, [1.75e4,1.3e4,7e3,4e3,2e3,1.5e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))

draw(PDF("Cu_Foil.pdf"),Cu_plot)
draw(PDF("Cu63Ni37_foil.pdf"), C63Ni37_plot)
draw(PDF("Cu26Ni74_pwdr.pdf"), Cu26Ni74_plot)
draw(PDF("Ni_Foil.pdf"), Ni_plot)
draw(PDF("CuNi_Unknown_Foil.pdf"), unknown_plot)

## Vegard's law
# make dataframe with lattice parameters
