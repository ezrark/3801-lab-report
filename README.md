# X-Ray Diffraction Lab Report
This lab report was written for the Structural Characterization of Materials (MATS 3801) class at the University of Minnesota. My hope in writing this was to demonstrate my ability in writing in LaTeX and performing data analysis. You'll find that the writing is clear and concise, and the tables and figures are high quality and easy to read. If you'd like a sample of my non-technical writing (from my Political Science minor), feel free to email me at ezra.kone@gmail.com.
### Writing Process
In this repository you'll find a folder for each of the three stages of the lab report writing process.
1. Stage one was a rough outline of the report. Many pieces are missing from this stage and there are numerous errors in the math. These issues are corrected later.
2. Stage two is a rough draft of the report, still with errors, but it shows the sections, figures, and data of the report.
3. The final stage is a completed product for submission. The figures are more refined and arranged better, the data tables are easier to read, and the prose provides a more refined analysis and conclusion of the experiment performed.

The report was written in LuaLaTeX using BibLaTeX and Zotero for reference management. All other packages used in LaTeX can be found in the .cls file for each stage and the main .tex file for each stage.
### Data and Figures
The data presented in tables and figures was analyzed using Microsoft Excel, LibreOffice Calc, and Julia. These citations and more information can be found in the various stages of the report. Figures were made in Julia using the Gadfly package and in OriginLab. If you'd like to see how the figures and analysis were done in Julia, all of my Julia code is in the Julia folder. The LibreOffice Calc spreadsheet is a little rougher in terms of data organization, which I learned is a serious detriment to the technical writing process.
