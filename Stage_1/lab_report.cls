\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lab_report}[2020/10/14 LaTeX Lab Report Class]
\LoadClass[titlepage]{article}

\RequirePackage[margin=1in]{geometry}
%\RequirePackage{fancyhdr}
\RequirePackage{amsmath}
\RequirePackage{graphicx}
\RequirePackage{array}
\RequirePackage{float}
\RequirePackage{enumitem}
\RequirePackage{pdfpages}
\RequirePackage[backend=biber,style=ieee]{biblatex}
\RequirePackage{siunitx}
\RequirePackage{tabularx}
\RequirePackage[toc,title]{appendix}
\RequirePackage[labelfont=bf]{caption}
\usepackage{subcaption,booktabs}

\newcommand{\pf}[2]{\left(\frac{#1}{#2}\right)}