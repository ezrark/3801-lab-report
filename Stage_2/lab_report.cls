\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lab_report}[2020/10/14 LaTeX Lab Report Class]
\LoadClass[titlepage,12pt]{article}

\RequirePackage[margin=1in]{geometry}
%\RequirePackage{fancyhdr}
\RequirePackage{amsmath}
\RequirePackage{graphicx}
\RequirePackage{array}
\RequirePackage{float}
\RequirePackage{enumitem}
\RequirePackage{pdfpages}
\RequirePackage[backend=biber,style=ieee]{biblatex}
\RequirePackage{siunitx}
\RequirePackage{tabularx}
\RequirePackage[toc,title]{appendix}
\RequirePackage{caption}
\RequirePackage{subcaption,booktabs}
\RequirePackage{fontspec}
\RequirePackage{subcaption,booktabs}
\RequirePackage{newtxtext,newtxmath}
%\RequirePackage{unicode-math}

\newcommand{\pf}[2]{\left(\frac{#1}{#2}\right)}
\setlength{\parindent}{4em}
\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.5}
%\setmainfont{TeX Gyre Termes}
%\setmathfont{texgyretermes-math.otf}[math-style=TeX]
\captionsetup{font={stretch=1.1},labelfont=bf}
\captionsetup[figure]{}