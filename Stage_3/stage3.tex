% !TeX root = ./stage3.tex
\documentclass{lab_report}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\numberwithin{table}{section}
\graphicspath{{/home/ezra/Documents/uni/20-21/3801_StructChar/Labs/XRD/}}
\addbibresource{MATS_3801_XRD.bib}
\author{Ezra Kone \\ TuM4}
\title{X-Ray Diffraction of Copper and Nickel Foils and Powders}

\begin{document}
\maketitle
\tableofcontents\pagebreak
\begin{abstract}
    The researchers wished to demonstrate the effectiveness and usefulness of x-ray diffraction (XRD) of metals. Samples of pure copper, pure nickel, two known CuNi alloys, and one unknown CuNi alloy were analyzed. Using Bragg's law to relate the diffraction peak intensity angles to lattice parameters and an extrapolation method to reduce error, the lattice parameter of each sample was determined. Additionally, using Vegard's law to relate composition and lattice parameter, the composition of the unknown alloy was determined. Each sample was analyzed once, which does not minimize error, but a systematic error of XRD was eliminated using an extrapolation method.

    The lattice parameters of the Cu foil, Cu$_{63}$Ni$_{37}$ foil, Cu$_{26}$Ni$_{74}$ powder, and Ni foil were $3.6175\pm0.0012$\si{\angstrom}, $3.5832\pm0.0002$\si{\angstrom}, $3.5475\pm0.0004$\si{\angstrom}, and $3.5255\pm0.00034$\si{\angstrom}, respectively. The unknown CuNi had a lattice parameter of $3.5588\pm0.0004$\si{\angstrom}, which corresponds to a Cu$_{37}$Ni$_{63}$ alloy. The standard uncertainty on the composition of the unknown sample was calculated to be $5.35*10^{-3}$mol\% Ni.
\end{abstract}
\section{Introduction}
Crystal lattices are ordered systems of atoms that make up solids--often metals. The layout of atoms in a material can define its properties, such as how and where it will deform when put under stress. This means knowing the crystal structure of a material is useful. A common way of determining the crystal structure of a material is through x-ray diffraction (XRD). By performing XRD on a sample, one can see the angles at which the material refracts incident x-rays, along with the intensity of those x-rays. This data pattern can be analyzed to yield the details of the crystal structure of the material.

The primary method of analysis for XRD patterns involves Bragg's law. Bragg's law relates the wavelength of peak XRD reflection intensity to the interplanar distance of atomic planes in a material's lattice \autocite{brandonMicrostructuralCharacterizationMaterials2013}. A diagram depicting the relationship between these values is shown in Figure \ref{fig:braggs_law}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{Braggs_law}
    \caption{A diagram depicting the relationship between incident angle and interplanar spacing that is used in Bragg's Law.\autocite{ConditionsDiffractionDerivation2006}}
    \label{fig:braggs_law}
\end{figure}

What the x-ray diffractometer measures is the angle at which an x-ray hits the sample $\theta$ and the intensity of the corresponding reflected x-ray. The intensity of the reflected x-ray is determined by the interference between the two reflected x-rays from the surface and initial underlying plane of the material. In the diagram, these are x-rays RB$_1$ and RB$_2$. If the extra distance that ray RB$_2$ travels is an integer multiple of the wavelength, then there will be constructive interference and a higher intensity of the reflected wave will be recorded.

The extra distance is $2d_{hkl}\sin(\theta)$ where $d_{hkl}$ is the distance between the two planes iof the crystal lattice. Setting the extra distance and constructive interference requirement equal to each other yields Bragg's law,
\begin{equation}
    n\lambda = 2d_{hkl}\sin(\theta).
    \label{eq:braggs_law}
\end{equation}
Although the interplanar spacing is a useful quantity to know, a more useful quantity--the lattice parameter--can be calculated from it. The lattice parameter describes the length of a side of the unit cell of a crystal lattice--in this case a cubic lattice, yielding only one lattice parameter. The lattice parameter $a$ in terms of the interplanar spacing is
\begin{equation}
    a = d_{hkl}\sqrt{h^2 + k^2 + l^2},
\end{equation}
where $h$, $k$, and $l$ represent the Miller indices of a given plane in the crystal lattice.

Each material structure has a unique lattice parameter, so knowing the lattice parameter can convey a lot of information about the material--even its overall identity. In this experiment, each sample will have its XRD pattern analyzed to yield a lattice parameter for each sample. To illustrate that a lattice parameter can be used to determine the material under analysis, a sample similar to other ones but with an unknown composition will be analyzed to calculate its molar composition.

The molar composition of a binary metallic alloy is expressed as M$_{x}$N$_{1-x}$. This is a linear relationship in which the increase of the amount of M present leads to a direct decrease in the relative amount of N present. If the lattice parameter of M is a$_M$ and the parameter of N is a$_N$, and an assumption is made that lattice parameters vary linearly, then there is logically a direct linear correspondence between composition and lattice parameter of an alloy composed of M and N. This correspondence is Vegard's Law \autocite{PhysRevA.43.3161}, and is given as
\begin{equation}
    a_{M_xN_{1 - x}} = xa_M + (1 - x)a_N.
\end{equation}
A more intuitive explanation of this is to say that a mixture of two pure elements has a lattice parameter that is also a mixture of the lattice parameters of the two elements.

This is the law that will be used in this experiment to demonstrate the ability to determine a material's composition using its XRD pattern. The samples analyzed will be pure copper and nickel foils, two CuNi alloys, and a foil of unknown CuNi composition. The composition of the unknown foil will be determined using a regression of the lattice parameters of the other four samples and interpolating to find the composition given its calculated lattice parameter.

The instrument used will be an x-ray diffractometer with graphite monochromator that allows pass-through of Cu K$_{\alpha_{1,2}}$ wavelengths \autocite{mannoMiniFlexQuickReference2020}. The monochromator ensures that only predetermined wavelengths impact the sample, which increases accuracy of calculations. The sample and will tilt on an attached goniometer with the x-ray detector. The detector will have slits in it to filter out wavelengths that are not the ones being measured--again, for increased accuracy of calculations. The x-ray source will not be mounted on the goniometer--it is fixed. This is the standard Bragg-Brentrano geometry \autocite{mannoXRDInstrumentationOverview}.

Although this experiment will be using a rather precise and accurate instrument, there will be some sources of error. These can be represented and accounted for using standard error analysis techniques, and one--sample height error--will be nearly eliminated using an extrapolation technique. The errors that are not the sample height error are random errors that will be analyzed using type A and B analyses from the Guide to Uncertainty in Measurement (GUM) \autocite{GUM}. Sample height error is a systematic error that has a correspondence with the equipment used and the angle of measurement of the x-ray and sample. More information on sample height error is presented in appendix \ref{app:param_extrap}.
\section{Procedure}
First and foremost, the experiment required an x-ray an diffractometer. This procedure used a Rigaku MiniFlex 600 with a 600W Cu x-ray source \autocite{mannoMiniFlexQuickReference2020}. The x-ray beam had a wavelength of $1.54059\pm0.00044$\si{\angstrom}. The diffractometer had multiple incident beam optics devices to filter and prepare the incident x-ray beam for diffraction on the sample. The purpose of these optical devices and the geometry of the instrument has been described in the Introduction. A full list is available in reference \autocite{mannoMiniFlexQuickReference2020}. 

At all times, safety goggles and nitrile gloves were worn. During loading and unloading of samples from the x-ray diffractometer, a radiation indicator ring was worn to indicate any possible exposure to leaked x-rays from the instrument. Additionally, a similar exposure detection badge was placed near the instrument to indicate any leakage not detected by the indicator ring or leakage that occurred outside of the sample loading and unloading periods.

The samples analyzed were a Cu foil, a Cu$_{63}$Ni$_{37}$ foil, a Cu$_{26}$Ni$_{74}$ powder, a Ni foil, and a foil of unknown composition Cu$_{1-x}$Ni$_{x}$. The foils were adhered into the well of a plastic sample stage using a putty and pressed down using a flat, hard object. The putty and pushing were done to allow the sample foil to sit as level as possible and at the height of the non-well portion of the sample stage. This helped minimize measurement error in the values reported from the instrument. The powder sample was loaded by dispensing a more-than-necessary amount of powder from a vial into the smaller well of a powder sample stage using a scupula. The excess powder was then scraped off the top of the stage using a similar hard, flat object to that used to level the foil samples.

Once loaded in the instrument, the sealed door of the instrument was locked to prevent opening during x-ray emission. The scan of the samples was then performed at angles $2\theta=30-115\si{\degree}$ in increments of \SI{0.04}{\degree}, dwelling 0.5s at each angle. The instrument used had a single point detector coupled on a goniometer to the beam generator. The goniometer had a radius of \SI{150}{\mm} and a minimum step size of \SI{0.01}{\degree}. The angle of the goniometer had accuracy $\Delta2\theta=\pm\SI{0.02}{\degree}$.
\section{Results and Discussion}
The data from the instrument were reported from the instrument in .xy files containing data points of angles $2\theta$ and their corresponding diffracted beam intensity. These data were analyzed in the MDI JADE software to produce peak angle fittings. These data were also plotted in Origin \autocite{Origin} as a single plot to show the differences in the XRD patterns across samples. This plot of the XRD patterns is shown in Figure \ref{fig:XRD_patterns} with labels of Miller Indices form source \autocite{PhysicalMethodsCharacterizing}. Overlaid on each plot in this figure are the Miller indices of the lattice plane that is attributed to that peak in the XRD pattern of a face-centered cubic crystal lattice, which Cu and Ni are.

While these plots of XRD patterns are useful, the most important values for each sample are shown in Table \ref{tab:peak_values}. This table displays the peak angles as fit by the MDI JADE software, their uncertainty as calculated in appendix \ref{app:lat_param}, their corresponding crystal plane as given by reference \autocite{PhysicalMethodsCharacterizing}, the interplanar spacing as an intermediate value calculated in Appendix \ref{app:lat_param}, and the lattice parameter calculated from each interplanar spacing and corresponding plane as described in the same appendix. There are no calculated uncertainties for the reference values from source \autocite{wyckoff1963a}.

Upon investigating Figure \ref{fig:XRD_patterns} and Table \ref{tab:peak_values}, it's seen that pure Ni has the highest peak angles and lowest lattice parameters, while pure Cu has lower peak angles and larger lattice parameters. The intermediate alloys display properties in between--consistent with the hypothesis of Vegard's law.
\begin{figure}[h]
    \centering
    \vspace{2em}
    \includegraphics[width=.8\textwidth]{Julia/XRD_patterns}
    \caption{XRD patterns for all samples overlaid on each other. The peaks on the plot are labeled with the Miller indices of their corresponding crystal lattice planes.}
    \label{fig:XRD_patterns}
\end{figure}
\pagebreak\begin{table}[H]
    \small
    \centering
    \caption{Peak angles and corresponding (hkl) planes, calculated interplanar distances and lattice parameters, and corresponding standard uncertainties for all samples and references.}
    \begin{tabular}{c|c|c|c|c|>{\centering\arraybackslash}p{6.5em}|c}
	\toprule
	\textbf{Sample} & \textbf{Peak Angle 2\theta\ [\si{\degree}]} & \textbf{(hkl) Plane} & \textbf{d} [\si{\angstrom}] & \textbf{u$_d$} [\si{\angstrom}] & \textbf{Lattice Parameter} [\si{\angstrom}] & \textbf{u$_a$} [\si{\angstrom}] \\
	\midrule
	 & 43.3171 & (111) & 1.12282 & -- & 3.61496 & -- \\
	 & 50.4496 & (200) & 0.999002 & -- & 3.61496 & -- \\
	Cu & 74.1265 & (220) & 0.800832 & -- & 3.61496 & -- \\
	reference & 89.938 & (311) & 0.770295 & -- & 3.61496 & -- \\
	 & 95.148 & (222) & 0.773415 & -- & 3.61496 & -- \\
     & 116.9345 & (400) & 0.864020 & -- & 3.61496 & -- \\
    \hline
	 & 43.3256 (0.0004) & (111) & 2.08671 & 0.00085 & 3.61429 & 0.00146 \\
	 & 50.4071 (0.0006) & (200) & 1.80890 & 0.00064 & 3.61781 & 0.00128 \\
	Cu & 74.105 (0.001) & (220) & 1.27840 & 0.00033 & 3.61586 & 0.00092 \\
	foil & 89.8716 (0.0017) & (311) & 1.09058 & 0.00024 & 3.61706 & 0.00080 \\
	 & 95.1253 (0.0047) & (222) & 1.04374 & 0.00023 & 3.61561 & 0.00079 \\
     & 116.7618 (0.0099) & (400) & 0.904578 & 0.000183 & 3.61831 & 0.00073 \\
    \midrule
	 & 43.6629 (0.0003) & (111) & 2.07137 & 0.00083 & 3.58771 & 0.00144 \\
	 & 50.8727 (0.0005) & (200) & 1.79344 & 0.00063 & 3.58687 & 0.00126 \\
    Cu$_{63}$Ni$_{37}$ & 74.8569 (0.0006) & (220) & 1.26741 & 0.00032 & 3.58478 & 0.00091 \\
	foil & 90.9075 (0.0011) & (311) & 1.08084 & 0.00024 & 3.58473 & 0.00079 \\
	 & 96.2104 (0.0036) & (222) & 1.03483 & 0.00022 & 3.58474 & 0.00078 \\
     & 118.5825 (0.0058) & (400) & 0.895927 & 0.000179 & 3.58371 & 0.00072 \\
    \midrule
	 & 43.8950 (0.0003) & (111) & 2.06095 & 0.00034 & 3.56967 & 0.00059 \\
     & 51.1754 (0.0004) & (200) & 1.78354 & 0.00029 & 3.56707 & 0.00059 \\
	Cu$_x$Ni$_{1-x}$ & 75.3833 (0.0006) & (220) & 1.25986 & 0.00021 & 3.56343 & 0.00059 \\
	unknown foil & 91.6447 (0.0009) & (311) & 1.07406 & 0.00018 & 3.56224 & 0.00059 \\
	 & 97.0304 (0.0016) & (222) & 1.02825 & 0.00017 & 3.56197 & 0.00059 \\
     & 119.9040 (0.0034) & (400) & 0.889891 & 0.000147 & 3.55956 & 0.00059 \\
    \midrule
	 & 44.0274 (0.0004) & (111) & 2.05506 & 0.00082 & 3.55947 & 0.00142 \\
    Cu$_{26}$Ni$_{74}$ & 51.3403 (0.0007) & (200) & 1.77819 & 0.00062 & 3.55639 & 0.00124 \\
	Powder & 75.6535 (0.0012) & (220) & 1.25603 & 0.00032 & 3.55260 & 0.00089 \\
	 & 92.0137 (0.0015) & (311) & 1.07071 & 0.00023 & 3.55115 & 0.00077 \\
     & 97.4548 (0.0028) & (222) & 1.02490 & 0.00022 & 3.55036 & 0.00075 \\
    \midrule
	 & 44.3753 (0.0003) & (111) & 2.03975 & 0.00034 & 3.53296 & 0.00058 \\
	 & 51.7256 (0.0005) & (200) & 1.76585 & 0.00029 & 3.53170 & 0.00058 \\
    Ni & 76.2595 (0.0006) & (220) & 1.24755 & 0.00021 & 3.52861 & 0.00058 \\
	foil & 92.7877 (0.0010) & (311) & 1.06380 & 0.00018 & 3.52822 & 0.00058 \\
	 & 98.3305 (0.0017) & (222) & 1.01810 & 0.00017 & 3.52681 & 0.00058 \\
     & 121.7872 (0.0037) & (400) & 0.881629 & 0.000145 & 3.52652 & 0.00058 \\
    \hline
	 & 44.4005 & (111) & 1.10094 & -- & 3.53105 & -- \\
	 & 51.7537 & (200) & 0.980822 & -- & 3.52991 & -- \\
    Ni & 76.2853 & (220) & 0.792902 & -- & 3.52760 & -- \\
	reference & 92.8411 & (311) & 0.771243 & -- & 3.52666 & -- \\
	 & 98.3459 & (222) & 0.778540 & -- & 3.52640 & -- \\
	 & 121.8466 & (400) & 0.906801 & -- & 3.52550 & -- \\
	\bottomrule
\end{tabular}
\label{tab:peak_values}
\end{table}\pagebreak

Compared to the reference angles from source \autocite{wyckoff1963a}, the Cu sample shows a similar lattice parameter at the lowest peak angle. The angle of this first peak is also very close on the analyzed sample and the reference. At higher angles, the Cu sample retains minimal deviation from the reference in terms of peak angle, but the lattice parameter at each peak diverges more from the reference parameters at these high angles. The Ni sample shows similar patterns to the Cu sample, with low angles being more accurate relative to the reference. However, in contrast to the Cu, the Ni reference shows increasing lattice parameters at increasing angles. This yields less divergence from the reference at high angles, as the reference itself is diverging from the true lattice parameter. This divergence from the true lattice parameter is of significant interest and should be looked at.
\begin{figure}[b!]
    \centering
    \vspace{2em}
    \includegraphics[width=.8\textwidth]{Julia/Cu63Ni37_params_origin}
    \caption{Calculated lattice parameter in Angstroms as a function of angle $\theta$. Error bars corresponding to $u_a$ in Table \ref{tab:peak_values} are shown. A linear fit line is shown. Calculations and plotting were performed in Origin \autocite{Origin}.}
    \label{fig:Cu63Ni37_params}
\end{figure}

Looking at all the peaks on the plot in Figure \ref{fig:XRD_patterns}, the peak angles become more dispersed as the angle of the peaks increases. This is due to sample height error, which is a systematic error that increases with peak angle. A visualization of this is shown for the Cu$_{63}$Ni$_{37}$ foil in Figure \ref{fig:Cu63Ni37_params}. This plot shows that as the angle increases on the x-axis, the calculated lattice parameter also increases. The error bars on the plot are due to the peak fitting process in MDI JADE combined with increasing uncertainty with increasing angle due to the Bragg's Law relationship. This error is more fully analyzed in Appendix \ref{app:lat_param}.

The linear fit line shown in Figure \ref{fig:Cu63Ni37_params} displays a relationship between the angle of the peak and the calculated lattice parameter from the interplanar spacing indicated by that peak. The sample analyzed has one true lattice parameter assuming a cubic crystal structure, so the lattice parameters calculated for each peak can be used to find the true lattice parameter. Using the extrapolation method described in source \autocite{brandonMicrostructuralCharacterizationMaterials2013} and explained in context in Appendix \ref{app:param_extrap}, the extrapolated lattice parameter for each sample is found. The extrapolated parameter is the y-intercept of the fit line as shown in Figure \ref{fig:Cu63Ni37_params}. Table \ref{tab:lat_params} shows the extrapolated lattice parameter for each sample, as calculated in Appendix \ref{app:param_extrap} and shown visually for Cu$_{63}$Ni$_{37}$ as the y-intercept of the fit line in Figure \ref{fig:Cu63Ni37_params}.
\begin{table}[h]
    \centering
    \caption{Extrapolated lattice parameters for all samples}
    \begin{tabular}{cc}
	\toprule
	\textbf{Sample} & \textbf{lattice parameter [\si{\angstrom}]} \\
	\midrule
	Cu foil & 3.6175\pm0.0012 \\
	Cu$_{63}$Ni$_{37}$ foil & 3.5832\pm0.0002 \\
	Cu$_{1-x}$Ni$_x$ foil & 3.5588\pm0.0004 \\
	Cu$_{26}$Ni$_{74}$ powder & 3.5475\pm0.0004 \\
	Ni foil & 3.5255\pm0.0003 \\
	\bottomrule
\end{tabular}
\label{tab:lat_params}
\end{table}
\begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Julia/vegard_params}
    \caption{Mole percent nickel in the sample vs. the extrapolated lattice parameter calculated according to appendix \ref{app:param_extrap}. The blue dots indicate samples of known composition, whereas the red dot indicates the sample of unknown composition plotted according to the regression performed using Vegard's law.}
    \label{fig:vegard_params}
\end{figure}

Table \ref{tab:lat_params} shows the extrapolated lattice parameter for each sample analyzed, including the sample of unknown CuNi composition. This table shows that as the mol\% of Ni increases, the lattice parameter decreases. To show this mathematically, Vegard's law is employed to devise a functional relationship between mol\% Ni and the extrapolated lattice parameter. The extrapolated parameter is used because each material only  has one lattice parameter, and the extrapolated one has a systematic error eliminated from it. A plot showing the relationship between lattice parameter and composition is shown in figure \ref{fig:vegard_params}. The red dot in the Figure indicates the sample of the unknown composition. As such, the x-value of this point was not calculated from a measurement, but was calculated from using Vegard's law from the extrapolated lattice parameter of the unknown sample. The unknown sample naturally did not contribute to the regression used to determine Vegard's law for Cu and Ni alloys. More information about the calculation of the composition of the unknown alloy can be found in Appendix \ref{app:vegard}.

The composition of the unknown alloy was found to be 63 mol\% Ni, or an alloy with molar formula Cu$_{37}$Ni$_{63}$. The standard uncertainty on this calculation is calculated in Appendix \ref{app:vegard} as $5.35*10^{-3}$mol\% Ni. This value is consistent with earlier observations that the unknown sample displayed XRD patterns in between those of the pure Cu and pure Ni samples and references.


\section{Conclusion}
The lattice parameter of the Cu foil was found to be $3.6175\pm0.0012$\si{\angstrom}, that of the Cu$_{63}$Ni$_{37}$ was found to be $3.5832\pm0.0002$\si{\angstrom}, the parameter of the Cu$_{26}$Ni$_{74}$ powder was $3.5475\pm0.0004$\si{\angstrom}, and the Ni foil's parameter was found to be $3.5255\pm0.00034$\si{\angstrom}. The unknown CuNi alloy was found to be a Cu$_{37}$Ni$_{63}$ alloy with lattice parameter $3.5588\pm0.0004$\si{\angstrom}. The standard uncertainty on the composition of the unknown sample was calculated to be $5.35*10^{-3}$mol\% Ni.

The sources of error were random and systematic, and the main source of systematic error--sample height--was eliminated via a linear regression of calculated data from measurements. The random error was not minimized or eliminated, but was calculated and has been represented in the results. If someone desired to minimize the random error of data collected and calculated from an XRD pattern, they could take multiple measurements of the same sample using the same scan parameters on the instrument. This would provide a larger sample to find averages on for peak positions--and therefore lattice parameters and composition if desired. This would most likely lead to values more similar to those in reference \autocite{wyckoff1963a}.

The use of Vegard's law would could be more accurate with more measurements of alloys of known composition in order to create a more accurate linear regression. Additionally, the regression could be made more accurate by weighting the points used by their relative uncertainties. This strategy of weighting values in a regression could also be useful in the process of extrapolating the lattice parameter.

Without weighting data points in the regression using their relative uncertainties, it's possible that values with higher uncertainty contributed too much to the regression's values given their volatility. This strategy would most likely need to be done with multiple XRD samplings of each sample to get a better understanding of the uncertainties.

The pure Cu and Ni samples had extrapolated lattice parameters very similar to those of the reference Cu and Ni foils. This informs us that although there was random error that could not be eliminated, the overall procedure was accurate and produced useful results.

The finding of the composition of the unknown alloy sample demonstrates the usefulness of XRD patterns. Using only one scan that took about 40 minutes, the composition and lattice parameter of the alloys were calculated. Knowing the identity of an alloy and its lattice parameter can be used to understand its mechanical and thermal properties in an engineering context, or for many other reasons in various sciences \autocite{smallmanPhysicalMetallurgyAdvanced2007}.
\pagebreak\begin{appendices}
    \section{Calculation of the Lattice Parameter}\label{app:lat_param}
    With the end goal being a value of the lattice parameter for each sample, we look at the full equation that gives the lattice parameter, which is derived from Bragg's law. The law states
    \begin{equation}\label{eq:bragg_law}
        n\lambda=2d_{hkl}\sin(\theta),
    \end{equation}
    where $\lambda$ is the wavelength us the x-ray used, $d_{hkl}$ is the interplanar spacing that corresponds with a given diffraction pattern peak, and $\theta$ is the diffraction angle, which is taken to be half the reported $2\theta$ value from MDI JADE; n is the diffraction order, which is assumed to be 1. The interplanar spacing is related to the lattice parameter $a$ by
    \begin{equation}\label{eq:lat_param}
        a=d_{hkl}\sqrt{h^2+k^2+l^2}
    \end{equation}
    and this relation can be used in equation \eqref{eq:bragg_law}. However, use of this relation prematurely to directly calculate the lattice parameter leads to unnecessarily complex differentiation to find the uncertainty of the lattice parameter. As such, calculations are done with the interplanar spacing as an intermediate value. Rearranging equation \eqref{eq:bragg_law} yields
    \begin{equation}
        d_{hkl}=\frac{n\lambda}{2\sin(\theta)}.
    \end{equation}
    The uncertainty of the interplanar spacing value is given by
    \begin{subequations}\label{eq:unc_d}
        \begin{align}
            u_d^2 & =\pf{\partial d}{\partial\lambda}^2u_\lambda^2+\pf{\partial d}{\partial\theta}^2u_\theta^2\\
            u_d^2 & =\pf{n^2}{4\sin(\theta)^2}u_\lambda^2+\frac{n^2\lambda^{2} \cos\left(\theta\right)^{2}}{4\sin\left(\theta\right)^{4}}u_\theta^2
        \end{align}
    \end{subequations}
    where $u_d, u_\lambda, \text{and } u_\theta$ are the standard uncertainties of their respective values as laid out previously. $u_\lambda$ is a quantity given by the quick reference manual for the Rigaku Miniflex 600 \autocite{mannoMiniFlexQuickReference2020} with standard error $\Delta\lambda=\SI{0.00044}{\angstrom}$. $u_\theta$ is a more complicated value, as the measured angle has three uncertainties associated with it. These are the resolution of the goniometer, the accuracy of the goniometer, and the error associated with the peak fitting process performed by the MDI JADE software. The resolution and accuracy are related to values presented in the Rigaku Miniflex quick reference manual. The standard uncertainties of these values will be found using a type B uncertainty analysis \autocite{GUM} that maps the uncertainty to a rectangular distribution. That is,
    \begin{subequations}
    \begin{align}
        u_{2\theta_{res}}^2 &= \frac{a_{res}^2}{3}\label{eq:u_theta_res}\\
        u_{2\theta_{acc}}^2 &= \frac{a_{acc}^2}{3}\label{eq:u_theta_acc}
    \end{align}
    \end{subequations}
    where the $a$ values are from the quick reference guide. The standard uncertainty on the position of $\theta$ is typically calculated as a type A measurement uncertainty analysis. In this case, the standard uncertainty on the position of the peak is
    \begin{equation}
        u_{2\theta_{pos}} = S_{\overline{x}} 
    \end{equation}
    as reported by the MDI JADE software in degrees. This will be converted to radians using the LibreOffice Calc software for calculations.
The aggregate uncertainty for the position of the peak is thus
\begin{subequations}\label{eq:u_theta_agg}
    \begin{align}
        u_{2\theta}^2 &= u_{2\theta_{res}}^2 + u_{2\theta_{a_c}}^2 + u_{2\theta_{pos}}^2\\
        u_{2\theta}^2 &= \frac{a_{res}^2}{3} + \frac{a_{acc}^2}{3} + S_{\overline{x}},
    \end{align}
\end{subequations}
which for the (111) peak of Cu foil is
\begin{subequations}\label{eq:u_CuF_111}
    \begin{align}
        u_{2\theta}^2 &= \pf{0.005^2}{3}^{\si{\degree}} + \pf{0.02^2}{3}^{\si{\degree}} + \SI{6.98E-6}{\radian}^2\\
        u_{2\theta}^2 &= \SI{4.32E - 8}{\radian^2},
    \end{align}
\end{subequations}
given that the reference manual states the minimum step size (resolution) of the goniometer is \SI{0.01}{\degree} and the accuracy of the goniometer is given as \pm\SI{0.02}{\degree}. At this point, it should be noted that equation \eqref{eq:unc_d} calls for $u_\theta^2$, while so far calculations have been performed for $u_{2\theta}^2$. To remedy this, $u_{2\theta}^2$ is simply divided by 2.

$S_{\overline{x}}$ is given by the MDI JADE software and is shown for all peak values of Cu foil in Table \ref{tab:peak_values}.
The standard uncertainty for \Delta\lambda\ is related to the standard error of the wavelength reported in the quick reference manual. It is found similarly to the standard uncertainties for $u_{2\theta}$. That is,
\begin{subequations}\label{eq:u_lambda}
    \begin{align}
        u_{\lambda}^2 &= \frac{a_{\lambda}^2}{3} \\
        &= \frac{\SI{0.00044}{\angstrom}^2}{3} \\
        u_{\lambda}^2 &=  \SI{6.45e-8}{\angstrom}.
    \end{align}
\end{subequations}
With $u_{2\theta}$ and $u_{\lambda}$ calculated for Cu foil's (111) peak, the full propagated standard uncertainty of the interplanar spacing can be calculated using equation \eqref{eq:unc_d} as
\begin{subequations}
    \begin{gather}
        u_d^2 =\pf{n^2}{4\sin(\theta)^2}u_\lambda^2+\frac{n^2\lambda^{2} \cos\left(\theta\right)^{2}}{4\sin\left(\theta\right)^{4}}u_\theta^2\\
        u_d^2 =\pf{1^2}{4\sin(\SI{21.6628}{\degree})^2}\SI{6.45e-8}{\angstrom} +\frac{1^2\SI{1.54059}{\angstrom}^2 \cos\left(\SI{21.6628}{\degree}\right)^{2}}{4\sin\left(\SI{21.6628}{\degree}\right)^{4}}\SI{4.32E - 8}{\radian}^2 \label{subeq:unc_d_Cu_111}\\
        u_d^2 = \SI{7.146E - 7}{\angstrom\squared}. \label{subeq:u_d_Cu_111_val}
    \end{gather}
\end{subequations}
With the uncertainty of the interplanar spacing determined, the uncertainty of the lattice parameter $a$ can be determined  from equation \eqref{eq:lat_param} using the same uncertainty propagation techniques that were employed for finding the uncertainty of the interplanar spacing $d$. That is,
\begin{subequations}
    \begin{align}
        u_a^2 &=  \left(\frac{\partial a}{\partial d}\right)^2u_d^2 \label{subeq:u_latparam_partials}\\
        u_a^2 &= \sqrt{h^2 + k^2 + l^2}^2\times u_d^2, \label{subeq:u_latparam_math}
    \end{align}
\end{subequations}
which for the (111) plane of Cu foil using the $u_d^2$ value from \eqref{subeq:u_d_Cu_111_val} is
\begin{subequations}
    \begin{align}\label{eq:u_Cu_111_latparam}
        u_a^2 &= (1^2 + 1^2 + 1^2)\times \SI{7.146E - 7}{\angstrom\squared}\\
        u_a^2 &= \SI{2.144E - 6}{\angstrom\squared}.\label{eq:Cu_param_unc_val}
    \end{align}
\end{subequations}
Returning to the original goal--to calculate the lattice parameter--equation \eqref{eq:lat_param} is used with the (111) interplanar spacing $d$ value given in Table \ref{tab:peak_values}, which yields
\begin{subequations}
    \begin{align}
        a &= \SI{2.087}{\angstrom}\sqrt{1^2 + 1^2 + 1^2}\\
        a &= \SI{3.614}{\angstrom}.\label{eq:Cu_param_val}
    \end{align}
\end{subequations}
Putting together the lattice parameter in equation \eqref{eq:Cu_param_val} and taking the square root of the standard uncertainty in equation \eqref{eq:Cu_param_unc_val} yields the value of the lattice parameter for the (111) plane of the Cu foil sample.
\begin{subequations}
    \begin{align}
        a &= \SI{3.614}{\angstrom}\pm\sqrt{\SI{2.144E 6}{\angstrom}}\\
        a &= 3.614\pm0.001\si{\angstrom}.
    \end{align}
\end{subequations}\pagebreak

\section{Lattice Parameter Extrapolation Method}\label{app:param_extrap}
In order to account for sample height error, an extrapolation method is used to find the true lattice parameter using the measured lattice parameters and their corresponding $\theta$ values. Using the process detailed in Brandon and Kaplan\autocite{brandonMicrostructuralCharacterizationMaterials2013},
\begin{subequations}
    \begin{align}
        a_{measured} &= \frac{a_{actual}2h}{R}\frac{\cos^2[\theta]}{\sin[\theta]}+a_{actual},\label{eq:param_extrap}
    \end{align}
\end{subequations}
where $a_{measured}$ is a given measured lattice parameter as detailed in appendix \ref{app:lat_param}, $h$ and $R$ are physical values related to the geometry of the goniometer, $\theta$ is the angle corresponding to the given lattice parameter, and $a_{actual}$ is the desired extrapolated lattice parameter with correction for sample height error. This relationship is made useful with a linear regression that reveals the term $\frac{a_{actual}2h}{R}\frac{\cos^2[\theta]}{\sin[\theta]}$ as the coefficient of the regression and $a_{actual}$ as the intercept. Using LibreOffice Calc to perform the regression on a 95\% confidence interval, an equation modeling the lattice parameter is found. For the ongoing example of Cu foil, this is
\begin{equation}
    a_{measured} = - 0.00089099\frac{\cos^2[\theta]}{\sin[\theta]} + 3.6175.\label{eq:param_extrap_val}
\end{equation}
The useful value here is the intercept, 3.6175, which corresponds with the actual lattice parameter adjusted for sample height error. LibreOffice Calc reports the standard error $\sigma$ of this intercept as 0.00114391. This is used to determine the 95\% confidence interval with the relationship
\begin{subequations}
    \begin{align}
        95\%(CI) &= 2.571\times\frac{\sigma}{\sqrt{n}}\label{subeq:95_CI}\\
        &= 2.571\times\frac{0.0011}{\sqrt{6}}\\
        95\%(CI) &= 0.0012,\label{subeq:95_CI_val}
    \end{align}
\end{subequations}
where $n$ is the number of samples, which in this case is 6. Bringing together the values from equations \eqref{eq:param_extrap_val} and \eqref{subeq:95_CI_val}, the calculated extrapolated lattice parameter for the Cu foil sample is
\begin{equation}
    a_{actual} = 3.6175\pm0.0012\si{\angstrom}.
\end{equation}

\section{Vegard's Law}\label{app:vegard}
The plot in Figure \ref{fig:known_params} shows the lattice parameters and corresponding mole \% of Ni in the samples analyzed. The blue line in this plot displays the result of a linear regression performed. The regression was performed in Julia \autocite{JuliaProgrammingLanguage}. The slope of the line is an arbitrary value that acts as a multiplier to find the lattice parameter or mole \% Ni given the other. The y-intercept is the lattice parameter of pure Cu, and the value at 1.0mol\% Ni is the lattice parameter of pure Ni.

As is stated in the introduction, Vegard's law is
\begin{equation}
    a_{M_xN_{1 - x}} = xa_M + (1 - x)a_N,
\end{equation}
where $a_{M_xN_{1 - x}}$ is the lattice parameter of any alloy of $M$ and $N$, $a_M$ is the lattice parameter of $M$, $a_N$ is the lattice parameter of $N$, and $x$ is the mol\% of $M$ in the alloy. This equation can be devised from the regression performed
\begin{figure}[b!]
    \centering
    \includegraphics[width=.8\textwidth]{Julia/known_params}
    \caption{Plot of extrapolated lattice parameter of the sample as a function of the sample's mol\% Ni. A regression line is shown as a visualization of Vegard's law.}
    \label{fig:known_params}
\end{figure}

The regression returns a slope $m$ and intercept $a_{Cu}$, yielding the equation
\begin{subequations}
    \begin{align}
        a_x &= m(x) + a_{Cu}\label{subeq:vegard_ax}\\
        x &=\frac{a_x - a_{Cu}}{m}\label{subeq:vegard_x},
    \end{align}
\end{subequations}
where $x$ is the mol\% Ni and $a_x$ is the lattice parameter of that composition. The symbolic standard uncertainty equation \eqref{subeq:vegard_x} is
\begin{equation}
        u_x^2 = \pf{\partial x}{\partial a_{Cu}}^2u_{a_{Cu}}^2 +\pf{\partial x}{\partial m}^2u_m^2 + \pf{\partial x}{\partial a_x}^2u_{a_x}^2.
\end{equation}
Taking these partial derivatives, one arrives at
\begin{subequations}
    \begin{align}
        u_x^2 =- \frac{1}{m^2}u_{a_{Cu}}^2 &- \frac{\left(a_{Cu} - a_x\right)^2}{m^4}u_m^2 + \frac{1}{m^2}u_{a_x}^2\\
        \notag\\u_x^2 = \frac{1}{.0926722^2}\left(2.144*10^{-6}\right) &- \frac{(3.618 - 3.559)^2}{(- 0.0926722)^4}\left(3.182*\frac{0.00141883}{\sqrt{4}}\right)^2 \notag\\
        &+\frac{1}{0.0926722^2}\left(2.571*\frac{0.00035606}{\sqrt{5}}\right)^2\\
        \notag\\u_x^2 = 2.87&*10^{-5} \text{mol\% Ni}^2\\
        u_x = 3.35&*10^{ - 3} \text{mol\% Ni}
    \end{align}
\end{subequations}
The standard uncertainty on the mol\% Ni is \SI{3.35e-3} mol\% Ni and the mol\% Ni is 0.63mol\% Ni.
\end{appendices}\pagebreak
\printbibliography
\end{document}